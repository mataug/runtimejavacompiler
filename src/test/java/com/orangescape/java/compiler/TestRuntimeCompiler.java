package com.orangescape.java.compiler;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.orangescape.java.compiler.RuntimeClassCompilerLoader.RuntimeCompiledClass;
@RunWith(JUnit4.class)
public class TestRuntimeCompiler {
	public static final RuntimeClassCompilerLoader compiler  = new RuntimeClassCompilerLoader();
	
	@Test
	public void testCanary(){
		assertTrue(true);
	}
	
	@Test(expected = ClassNotFoundException.class)
	public void testCompileInvalidSource() throws MalformedURLException, ClassNotFoundException{
			compiler.compile("", "");
	}
	
	@Test(expected = NullPointerException.class)
	public void testCompileInvalidJavaObjects() {
		compiler.compile(null);
	}
	
	@Test(expected = NullPointerException.class)
	public void testInvalidRuntimeCompiledClass() {
		new RuntimeCompiledClass(null);
	}

	@Test
	public void testCompileValidSource() throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		StringBuilder builder = new StringBuilder();
		builder.append("package test;");
		builder.append("public class TestClass implements com.orangescape.java.InstanceMethod {");
		builder.append("	public void setUp(){ ");
		builder.append("		System.out.println(\"From Test Class setUp\");");
		builder.append("	}");
		builder.append("	public void tearDown(){ ");
		builder.append("		System.out.println(\"From Test Class tearDown\");");
		builder.append("	}");
		builder.append("	public void run(){ ");
		builder.append("		System.out.println(\"From Test Class run\");");
		builder.append("	}");
		builder.append("}");
		RuntimeCompiledClass runtimeClass = null;
		try {
			runtimeClass = compiler.compile(builder.toString(),"test.TestClass");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("test.TestClass",runtimeClass.getThisClass().getName());
		runtimeClass.invoke("setUp");
		runtimeClass.invoke("run");
		runtimeClass.invoke("tearDown");
	}
	 
	
	

	
	

}
