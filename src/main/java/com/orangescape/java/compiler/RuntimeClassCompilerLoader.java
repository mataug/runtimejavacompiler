package com.orangescape.java.compiler;


import static com.google.common.base.Preconditions.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import com.google.common.io.Files;

public class RuntimeClassCompilerLoader{
	public static class SimpleDiagnosticListener implements
			DiagnosticListener<JavaFileObject> {
		public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
			System.out.println("Line Number->" + diagnostic.getLineNumber());
			System.out.println("code->" + diagnostic.getCode());
			System.out.println("Message->"
					+ diagnostic.getMessage(Locale.ENGLISH));
			System.out.println("Source->" + diagnostic.getSource());
			System.out.println(" ");
		}
	}
	
	/**
	 * java File Object represents an in-memory java source file <br>
	 **/
	public static class InMemoryJavaFileObject extends SimpleJavaFileObject {
		private String contents = null;

		public InMemoryJavaFileObject(String className, String contents){
			super(URI.create("string:///" + className.replace('.', '/')
					+ Kind.SOURCE.extension), Kind.SOURCE);
			this.contents = contents;
		}

		public CharSequence getCharContent(boolean ignoreEncodingErrors)
				throws IOException {
			return contents;
		}
	}
	
	/**
	 * A container for java.lang.Class that is obtained when a class is loaded
	 * <br>
	 * Offers abstractions for invoking methods
	 * @author gautam
	 *
	 */
	public static class RuntimeCompiledClass {

		private final Class<?> thisClass;

		public RuntimeCompiledClass(Class<?> thisClass)
				throws IllegalArgumentException {
			super();
			checkNotNull(thisClass);
			this.thisClass = thisClass;
		}
		/**
		 * Invoke a method on the loaded class without parameters
		 * @param methodName
		 * @throws InstantiationException
		 * @throws IllegalAccessException
		 * @throws NoSuchMethodException
		 * @throws SecurityException
		 * @throws IllegalArgumentException
		 * @throws InvocationTargetException
		 */
		public void invoke(String methodName) 
				throws 	InstantiationException, 
						IllegalAccessException, 
						NoSuchMethodException, 
						SecurityException, 
						IllegalArgumentException, 
						InvocationTargetException {
			Object instance = thisClass.newInstance();
			Method thisMethod = thisClass.getDeclaredMethod(methodName, new Class[] {});
			thisMethod.invoke(instance, new Object[] {});
		}

		/**
		 * returns the class loaded along with the instance
		 * @return Class
		 */
		public Class<?> getThisClass() {
			return thisClass;
		}
	}
	
	public final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
	public final SimpleDiagnosticListener diagnosticListener = new SimpleDiagnosticListener();
	public final StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnosticListener, Locale.ENGLISH, null);
	public final String outputDirectory = Files.createTempDir().getAbsolutePath();
	private ClassLoader loader;

	public Boolean compile(Iterable<? extends JavaFileObject> files) {
		checkNotNull(files);
		Iterable<String> options = Arrays.asList("-d",outputDirectory); 
		JavaCompiler.CompilationTask compilationTask = 
			compiler.getTask(
				null,fileManager, 
				diagnosticListener,options, 
				null, files);
		return compilationTask.call();
	}
	
	
	public RuntimeCompiledClass loadClass(String classToLoad) 
			throws 	MalformedURLException, 
					ClassNotFoundException {
		File outputDirectory = new File(this.outputDirectory);
		URL url = outputDirectory.toURI().toURL();
		URL[] urls = new URL[] {url};
		loader = new URLClassLoader(urls);

		Class<?> thisClass = loader.loadClass(classToLoad);
		return new RuntimeCompiledClass(thisClass);
	}
	
	public RuntimeCompiledClass compile(String source , String classToLoad) throws MalformedURLException,ClassNotFoundException {
		JavaFileObject fileObject = new InMemoryJavaFileObject(classToLoad, source);
		checkArgument(compile(Arrays.asList(fileObject)));
		return loadClass(classToLoad);
	}
}

