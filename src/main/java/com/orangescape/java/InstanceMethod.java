package com.orangescape.java;

public interface InstanceMethod extends Runnable {
	public void setUp();
	public void tearDown();
}
